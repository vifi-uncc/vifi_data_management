#!/bin/bash

rm -f deploy/global/*
rm -f deploy/local/*
cp target/metadata*with*dep*.jar metadata.jar

mkdir deploy/global -p
mkdir deploy/local -p
mkdir deploy/client -p


cp metadata.jar deploy/global
cp metadata.jar deploy/local
cp metadata.jar deploy/client

rm -f metadata.jar

cp scripts/metadata.properties.local deploy/global/metadata.properties
cp scripts/metadata.properties.local deploy/local/metadata.properties
cp scripts/metadata.properties.local deploy/client/metadata.properties

cp scripts/*.sh deploy/global
cp scripts/*.sh deploy/local
cp scripts/*.sh deploy/client
cp scripts/*xslt deploy/global
cp scripts/*xslt deploy/local
cp scripts/*xslt deploy/client

rm deploy/local/globalserver.sh
rm deploy/global/localserver.sh
rm deploy/client/globalserver.sh
rm deploy/client/localserver.sh


