package edu.uncc.vislab.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.swing.tree.ExpandVetoException;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import org.apache.commons.cli.*;

public class Annontate {

	private static BufferedReader getOutput(Process p) {
		return new BufferedReader(new InputStreamReader(p.getInputStream()));
	}

	private static BufferedReader getError(Process p) {
		return new BufferedReader(new InputStreamReader(p.getErrorStream()));
	}

	static String outputFilePath;
	static String inputFilePath;
	static String runCMD;

	static void parse(String args[]) throws javax.xml.parsers.ParserConfigurationException {

		Options options = new Options();

		Option input_ = new Option("i", "input", true, "input file path");
		input_.setRequired(true);
		options.addOption(input_);

		Option command_ = new Option("c", "cmd", true, "command to run");
		command_.setRequired(true);
		options.addOption(command_);

		Option output_ = new Option("o", "output", true, "output file  ");
		output_.setRequired(true);
		options.addOption(output_);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);

			outputFilePath = cmd.getOptionValue("output");
			inputFilePath = cmd.getOptionValue("input");
			runCMD = cmd.getOptionValue("cmd");
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
			return;
		}
	}

	public static File readfile(String name) {
		String cmd = runCMD + " " + name + "";
		try {
			Process p = Runtime.getRuntime().exec(cmd);
			BufferedReader output = getOutput(p);
			File tempFile2 = File.createTempFile("tempfile", "metadata" + name.replace("/", "_"));
			tempFile2.deleteOnExit();
			BufferedWriter tempfile = new BufferedWriter(new FileWriter(tempFile2));
			char buf[] = new char[100];

			for (;;) {
				int x = output.read(buf);
				if (x == -1)
					break;
				System.out.println(buf);
				tempfile.write(buf, 0, x);
			}
			p.waitFor();
			tempfile.close();
			return tempFile2;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int add(String srcf, String resf) {
		int sucess=0;
		try {
			File fXmlFile = new File(srcf);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			NodeList nList = doc.getElementsByTagName("File");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				Element e = (Element) nNode;
				String fname = e.getAttribute("name");
				try{
				File tmpfile = readfile(fname);
				Document doc2 = dBuilder.parse(tmpfile);
				NodeList metadatas = doc2.getElementsByTagName("metadata");
				for (int i = 0; i < metadatas.getLength(); i++) {
					Element metadata_ = (Element) doc.importNode(metadatas.item(i), true);
					e.appendChild(metadata_);
				}
				}catch(Exception ee){
					ee.printStackTrace();
				}
			}
			prettyPrint(doc, resf);
		} catch (Exception e) {
			e.printStackTrace();
			 sucess=1;	
		}
		return  sucess;
	}

	public static final void prettyPrint(Document xml, String output) throws Exception {
		Transformer tf = TransformerFactory.newInstance().newTransformer();
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");
		tf.transform(new DOMSource(xml), new StreamResult(new FileWriter(output)));
	}
	public static final void prettyPrint(String input, String output) throws Exception {
		
		File fXmlFile = new File(input);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
		prettyPrint(doc, output);

	}
	public static void run(String input, String output, String cmd) {
		try {
			runCMD = cmd;
			int t=add(input, output);
			if(t==1){
				System.err.println("error");
				prettyPrint(input,output);				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		try {
			parse(args);
		} catch (Exception e) {
			// TODO: handle exception
		}
		run(inputFilePath, outputFilePath, runCMD);
	}

}
