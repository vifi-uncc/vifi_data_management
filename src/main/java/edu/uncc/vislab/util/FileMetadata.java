package edu.uncc.vislab.util;

import java.io.File;
import java.io.StringWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.apache.commons.cli.*;

public class FileMetadata {

	static String inputFilePath;

	static void parse(String args[]) {

		Options options = new Options();

		Option input = new Option("i", "input", true, "input file path");
		input.setRequired(true);
		options.addOption(input);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
			inputFilePath = cmd.getOptionValue("input");
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
			return;
		}
	}

	static public String convertTime(long time) {
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		return format.format(date);
	}

	public static void main(String args[]) {
		parse(args);
		// https://www.mkyong.com/java/how-to-create-xml-file-in-java-dom/
		File file = new File(inputFilePath);
		String access = (file.canExecute() ? "x" : " ") + (file.canWrite() ? "w" : " ") + (file.canRead() ? "r" : " ");
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element metadata = doc.createElement("metadata");
		//	metadata.setAttribute("id", id);
			metadata.setAttribute("type", "file");
			metadata.setAttribute("name", file.getAbsolutePath());
			String fs = Long.toString(file.length());
			metadata.setAttribute("size", fs);
			metadata.setAttribute("lastupdated", convertTime(file.lastModified()));
			metadata.setAttribute("access",access);
			doc.appendChild(metadata);
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(metadata);
			transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
			StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (Exception e) {
		}
	}
}
