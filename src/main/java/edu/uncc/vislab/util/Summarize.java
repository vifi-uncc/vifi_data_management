package edu.uncc.vislab.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;

import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import edu.uncc.vislab.SolrSchema;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.apache.commons.cli.*;

public class Summarize {

	static String outputFilePath;
	static String inputFilePath;

	static void parse(String args[]) throws javax.xml.parsers.ParserConfigurationException {

		Options options = new Options();

		Option input_ = new Option("i", "input", true, "input file path");
		input_.setRequired(true);
		options.addOption(input_);

		Option output_ = new Option("o", "output", true, "output file path");
		output_.setRequired(true);
		options.addOption(output_);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);

			outputFilePath = cmd.getOptionValue("output");
			inputFilePath = cmd.getOptionValue("input");

		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
			return;
		}
	}

	public static void process(String srcf, String resf) {
		try {
			File fXmlFile = new File(srcf);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			NodeList nList = doc.getElementsByTagName("metadata");

			Document d = dBuilder.newDocument();

			Node root = d.createElement("Dataset");
			d.appendChild(root);
			Node mm = d.createElement("metadata");
			root.appendChild(mm);
			HashMap<String, Node> nodes = new HashMap<>();
			for (String s : SolrSchema.getSchema()) {
				Node n = d.createElement(s);
				mm.appendChild(n);
				nodes.put(s, n);
			}

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node n = nList.item(temp);
				NamedNodeMap attrs = n.getAttributes();
				// get type
				Node t = attrs.getNamedItem("type");
				String type = "";
				if (t != null)
					type = t.getNodeValue();
				for (int i = 0; i < attrs.getLength(); i++) {
					Attr attribute = (Attr) attrs.item(i);
					String col = attribute.getName();
					String val = attribute.getValue();

					if (col.equals("name") & type.equals("file"))
						continue;

					Node n2 = d.createElement("values");
					n2.appendChild(d.createTextNode(val));

					Node n1 = nodes.get(col);
					if (n1 != null)
						n1.appendChild(n2);

				}
			}
			Node org = d.importNode(doc.getDocumentElement(), true);
			Node org_d = d.createElement("org");
			org_d.appendChild(org);
			root.appendChild(org_d);

			Summarize(d, resf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final void Summarize(Document xml, String output) throws Exception {
		Source xslt = new StreamSource(new File("transform.xslt"));
		Transformer tf = TransformerFactory.newInstance().newTransformer(xslt);
		tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		tf.setOutputProperty(OutputKeys.INDENT, "yes");

		tf.transform(new DOMSource(xml), new StreamResult(new FileWriter(output)));
	}

	public static void run(String input, String output) {
		try {

			process(input, output);

		} catch (Exception e) {
		}
	}

	public static void main(String[] args) {
		try {
			parse(args);
			run(inputFilePath,outputFilePath);
		} catch (Exception e) {
			// TODO: handle exception
		}
		run(inputFilePath, outputFilePath);
	}

}
