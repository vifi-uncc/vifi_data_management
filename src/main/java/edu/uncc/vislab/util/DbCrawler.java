package edu.uncc.vislab.util;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import java.io.File;
import java.io.StringWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.apache.commons.cli.*;

//command line
//psql -U postgres -h localhost -p 5432

public class DbCrawler {

	static String outputFilePath;
	static String id;
	static String db;

	static void parse(String args[]) throws javax.xml.parsers.ParserConfigurationException {

		Options options = new Options();

		Option output = new Option("o", "output", true, "output file path");
		output.setRequired(true);
		options.addOption(output);

		Option idoption = new Option("d", "id", true, "id");
		idoption.setRequired(false);
		options.addOption(idoption);

		Option database = new Option("db", "database", true, "database ");
		database.setRequired(true);
		options.addOption(database);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
			db = cmd.getOptionValue("db");
			outputFilePath = cmd.getOptionValue("output");
			id = cmd.getOptionValue("id");

		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
			return;
		}
	}

	public static void main(String[] args) {

		try {
			parse(args);
		} catch (ParserConfigurationException e1) {
			return;
		}
		Connection conn = null;
		try {

			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/" + db, "postgres", "");

		} catch (RuntimeException e) {
			conn = null;
		} catch (ClassNotFoundException e) {

		} catch (SQLException e) {

		}

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();
			// root elements
			Document doc = docBuilder.newDocument();
			Element database = doc.createElement("Database");
			if (conn != null) {
				Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery(
						"SELECT table_name, column_name FROM information_schema.columns " + "where table_catalog ='"
								+ db + "'" + " and table_schema='public'" + "order by  table_name, column_name ");

				Element table = doc.createElement("Table");
				int table_n = 0;
				String prev_table_name = "";
				while (rs.next()) {
					String table_name = rs.getString(1);
					String col_name = rs.getString(2);
					// System.out.println(table_name + " " + col_name);
					if (table_name.equals(prev_table_name)) {
						Element column = doc.createElement("Column");
						Element metadata = doc.createElement("metadata");
						metadata.setAttribute("id", id);
						metadata.setAttribute("type", "column");
						metadata.setAttribute("name", col_name);
						metadata.setAttribute("table_name", table_name);
						column.appendChild(metadata);
						table.appendChild(column);
						table_n++;
					} else {
						database.appendChild(table);
						table_n = 0;
						table = doc.createElement("Table");
					}
					prev_table_name = table_name;
				}
				if (table_n > 0) {
					database.appendChild(table);
				}

				rs.close();
				st.close();
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(database);
			StreamResult result = new StreamResult(new File(outputFilePath));
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {

		} catch (SQLException e) {

		} catch (TransformerConfigurationException e) {

		} catch (TransformerException e) {
		}
	}
}
