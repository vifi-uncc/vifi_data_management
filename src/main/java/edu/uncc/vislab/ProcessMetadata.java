package edu.uncc.vislab;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import org.apache.solr.common.SolrInputDocument;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ProcessMetadata {

	private Log log = LogFactory.getLog(ProcessMetadata.class);

	public static String[] getIds(Element element) {
		String id = element.getAttribute("id");
		String parentid = element.getAttribute("parentid");
		String datasetid = element.getAttribute("datasetid");
		String[] ids = new String[3];
		ids[0] = id;
		ids[1] = parentid;
		ids[2] = datasetid;
		return ids;
	}

	public static void parseXpath(String name) throws Exception{

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new File(name));
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();

		SolrInputDocument solrdoc = new SolrInputDocument();
		ArrayList<String> keywordsArray = new ArrayList<>();
		ArrayList<String> valuesArray = new ArrayList<>();

		for (String s : SolrSchema.getSchema()) {
			XPathExpression expr = xpath.compile("//Dataset//metadata/" + s+"/values");
			NodeList nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {

				Element element = (Element) nodes.item(i);

				solrdoc.addField(s, element.getTextContent().trim());
				System.out.println( "key:"+ s+ "value "+element.getTextContent());
				//System.out.println( "element "+ element);
			}
		}
		SolrMetadataClient client = new SolrMetadataClient();
		client.add(solrdoc);
	}

	public static void parse2(String name) throws Exception {

		SolrMetadataClient client = new SolrMetadataClient();
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(new File(name));

		/*
		 * NodeList dataset = doc.getElementsByTagName("Dataset");
		 * 
		 * for (int i = 0; i < dataset.getLength(); i++) { Element element =
		 * (Element) dataset.item(i);
		 * 
		 * SolrInputDocument solrdoc = new SolrInputDocument();
		 * solrdoc.addField("id", element.getAttribute("id"));
		 * solrdoc.addField("name", element.getAttribute("name"));
		 * solrdoc.addField("type", "dataset"); client.add(solrdoc); }
		 */

		NodeList nodes = doc.getElementsByTagName("Dataset");

		SolrInputDocument solrdoc = new SolrInputDocument();
		ArrayList<String> keywordsArray = new ArrayList<>();
		ArrayList<String> valuesArray = new ArrayList<>();
		for (int i = 0; i < nodes.getLength(); i++) {
			System.out.println("adding Dataset");
			Element element = (Element) nodes.item(i);

			NodeList children = element.getChildNodes();
			for (int j = 0; j < children.getLength(); j++) {
				Node child = (Node) children.item(j);
				if (child instanceof CharacterData) {

					CharacterData cd = (CharacterData) child;

					String t = cd.getData().trim();
					if (!t.isEmpty()) {
						System.out.println("CDATA Document " + t);
						SolrMetadataClient.getKeyVal(t, solrdoc, keywordsArray, valuesArray);
					}
				}
			}
			// solrdoc.addField("K", keywordsArray);
			// solrdoc.addField("V", valuesArray);

			client.add(solrdoc);
		}

		/*
		 * NodeList cols = doc.getElementsByTagName("cols"); for (int i = 0; i <
		 * cols.getLength(); i++) { Element element = (Element) cols.item(i);
		 * String document = getCharacterDataFromElement(element); String[] ids
		 * = getIds(element); SolrInputDocument solrdoc =
		 * SolrMetadataClient.Addcols(document, ids); client.add(solrdoc); }
		 */
	}

	public static String getCharacterDataFromElement(Element e) {
		String json = "";
		NodeList children = e.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node child = (Node) children.item(i);
			if (child instanceof CharacterData) {

				CharacterData cd = (CharacterData) child;

				String t = cd.getData().trim();
				if (!t.isEmpty())
					json = t;
			}
		}
		System.out.println("child" + json);
		return json;
	}

	public static void main(String[] args) throws Exception {
		parseXpath(args[0]);
	}

	public static void run(String name) throws Exception {
		parseXpath(name);
	}
}
