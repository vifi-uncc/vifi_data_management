package edu.uncc.vislab;

import java.io.*;
import java.util.*;

import static spark.Spark.*;

public class LocalMetadataServer extends MetadataServer {

	
	static void id() {

		get("/id", (request, response) -> {

		return "local Metadata Server";
			
		});
	}
	public static void main(String[] args) {
		int portip = Conf.getInt("localserver.port");
		dir=Conf.getString("metadata.dir");
		System.out.println(portip);
		port(portip);
		metadata();
		dataset();
		list();
		id();
	}
}
