package edu.uncc.vislab;

import java.io.*;
import java.util.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Timestamp;

import static spark.Spark.*;
import javax.servlet.*;
import javax.servlet.http.Part;

import spark.Request;
import spark.Response;
import spark.Route;

public class MetadataServer {

	static private Log log = LogFactory.getLog(MetadataServer.class);
	public static String dir = "";
	public static int id = 0;

	static class Data {
		public String name;
		public String path;
		public int id = 0;

		public Data(int id, String name, String path) {
			this.id = id;
			this.name = name;
			this.path = path;
		}

		@Override
		public String toString() {
			return name + " " + path;
		}
	}

	static Map<Integer, Data> datasets = new HashMap<>();

	static String print() {
		String rc = "";
		Map mp = datasets;
		Iterator it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			rc = rc + (pair.getKey() + " = " + pair.getValue()) + "<br/>";
		}
		return rc;
	}

	static void add(String n, String p, Map<String, String> params) {
		datasets.put(id++, new Data(id, n, p));
		(new Thread(new BackgroundWork(n, p, params))).start();
	}

	private static boolean saveStream(String name, InputStream s) {
		try {
			BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(name));
			byte[] buffer = new byte[1024];
			int len = 0;
			while ((len = s.read(buffer)) > 0) {
				out.write(buffer, 0, len);
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	static void id() {

		get("/id", (request, response) -> {

			return " Metadata Server";

		});
	}

	static String AddMetadata(String name, Request req, Response res) {

		MultipartConfigElement multipartConfigElement = new MultipartConfigElement("");
		req.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);

		log.info("metadata " + "add" + name);
		try {

			for (Part v : req.raw().getParts()) {
				if (v.getSubmittedFileName() != null) {

					long unixTime = System.currentTimeMillis();

					String n = dir + name + "-" + unixTime + ".xml";
					System.out.println("name::::::" + n);
					log.warn("name::::::" + n);
					saveStream(n, v.getInputStream());
					ProcessMetadata.run(n);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String ret = "Metadata for " + name + " has been recieved and indexed";
		return ret;

	}

	static void metadata_form() {

		post("/metadata/formadd", (request, response) -> {

			String name = request.queryParams("name");
			if (name == null)
				name = "empty";
			return AddMetadata(name, request, response);
		});
	}

	static void metadata() {

		post("/metadata/add/:name", (request, response) -> {

			String name = request.params(":name");
			return AddMetadata(name, request, response);
		});
	}

	static Map<String, String> compute(Request r) {
		Map<String, String> mp = new HashMap<>();
		Iterator it = r.params().entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			String k = (String) pair.getKey();
			if (k.startsWith(":")) {
				k = k.replaceAll(":", "");
			}
			mp.put(k, (String) pair.getValue());
		}
		for (String s : r.queryParams()) {
			String value = "";
			value += r.queryParams(s) + "";
			mp.put(s, value);
		}
		return mp;
	}

	static void search() {
		post("/metadata/search", (request, response) -> {

			MultipartConfigElement multipartConfigElement = new MultipartConfigElement("");
			request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);
			Map<String, String> params = compute(request);
			List<String> r = Search.run(params.values().toArray(new String[params.values().size()]));

			String m = "";
			for (String t : r) {
				m = m + t + "<br/>";
			}
			return m;
		});
	}

	static void dataset() {
		post("/dataset/add/:name", (request, response) -> {

			MultipartConfigElement multipartConfigElement = new MultipartConfigElement("");
			request.raw().setAttribute("org.eclipse.jetty.multipartConfig", multipartConfigElement);

			String name = request.params(":name");
			String path = "";
			if (request.queryParams().contains("path"))
				path = request.queryParams("path");

			log.info("dataset " + "add" + name + " " + path);

			add(name, path, compute(request));

			return id;
		});
	}

	static void list() {
		get("/list", (request, response) -> {
			return print();
		});

	}
}
