#!/bin/bash

for H in A B C metadata; do 

	ssh -i "uncc-poc.pem" -t ubuntu@$H " 
		sudo rm -fr /vifi/metadata
		sudo mkdir /vifi/data -p
 		sudo mkdir /vifi/metadata -p
		
		sudo chown ubuntu /vifi/data -R
		sudo chgrp ubuntu /vifi/data -R

		sudo chown ubuntu /vifi/ -R
		sudo chgrp ubuntu /vifi/ -R
	"
done	


scp -i "uncc-poc.pem" -r deploy/client/* ubuntu@user:/vifi/metadata
scp -i "uncc-poc.pem" -r deploy/global/* ubuntu@metadata:/vifi/metadata
scp -i "uncc-poc.pem" -r deploy/local/* ubuntu@B:/vifi/metadata
scp -i "uncc-poc.pem" -r deploy/local/* ubuntu@C:/vifi/metadata


scp -i "uncc-poc.pem" scripts/metadata.properties.global ubuntu@user:/vifi/metadata/metadata.properties
scp -i "uncc-poc.pem" scripts/metadata.properties.global ubuntu@metadata:/vifi/metadata/metadata.properties
scp -i "uncc-poc.pem" scripts/metadata.properties.global ubuntu@B:/vifi/metadata/metadata.properties
scp -i "uncc-poc.pem" scripts/metadata.properties.global ubuntu@C:/vifi/metadata/metadata.properties


ssh -i "uncc-poc.pem" -t ubuntu@user " 
  sudo rm -fr /vifi/web
  sudo mkdir /vifi/web
  sudo chown ubuntu /vifi/ -R
  sudo chgrp ubuntu /vifi/ -R

"


scp -i "uncc-poc.pem" -r web/* ubuntu@user:/vifi/web




ssh -i "uncc-poc.pem" -t ubuntu@user " 
  docker pull  httpd    
  
"

