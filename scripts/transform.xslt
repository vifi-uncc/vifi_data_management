<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output omit-xml-declaration="yes" indent="yes"/>

 <xsl:template match="node()|@*">
   <xsl:copy>
    <xsl:apply-templates select="node()|@*"/>
   </xsl:copy>
 </xsl:template>

 <xsl:template match="Dataset/metadata/size">
   <size>
   	<values>
    <xsl:value-of select="sum(values)"/>
</values>
   </size>

 </xsl:template>

<xsl:template match="Dataset/metadata/type">
<type>

<xsl:for-each select="values[not(preceding::values/. = .)]">
                <values><xsl:value-of select="." /></values>
        </xsl:for-each> 
</type>

</xsl:template>


<xsl:template match="Dataset/metadata/lastupdated">
<lastupdated>
<values>
<xsl:variable name="latest">
          <xsl:for-each select="values">
            <xsl:sort select="." order="descending" />
            <xsl:if test="position() = 1">
              <xsl:value-of select="."/>
            </xsl:if>
          </xsl:for-each>
        </xsl:variable>
      <xsl:value-of select="$latest"/>
</values>
</lastupdated>

</xsl:template>

</xsl:stylesheet>
